package com.tngtech.jira.plugins.schizophrenia.component;

import net.java.ao.DBParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.tngtech.jira.plugins.schizophrenia.ao.SwitchUserConfig;

@Service
public class SwitchUserConfigService {
    
    private Integer ID = 1;

    @Autowired
    private ActiveObjects entityManager;

    public void storeConfiguration(final String groupAllowedToSwitch, final String assignableGroup) {
        SwitchUserConfig switchUserConfig = getValidConfiguration();
        switchUserConfig.setGroupAllowedToSwitch(groupAllowedToSwitch);
        switchUserConfig.setAssignableGroup(assignableGroup);
        switchUserConfig.save();
    }

    public SwitchUserConfig getValidConfiguration() {
        SwitchUserConfig switchUserConfig = entityManager.get(SwitchUserConfig.class, ID);
        
        if(switchUserConfig == null) {
            switchUserConfig = entityManager.create(SwitchUserConfig.class, new DBParam("IDENTIFIER", ID));
            switchUserConfig.setAssignableGroup("");
            switchUserConfig.setGroupAllowedToSwitch("");
            switchUserConfig.save();
        }
        
        return switchUserConfig;
    }
}
