package com.tngtech.jira.plugins.schizophrenia.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "configuration")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserSwitchConfiguration {

	@XmlElement
	private String assignableGroup;

	@XmlElement
	private String groupAllowedToSwitch;

	public String getAssignableGroup() {
		return assignableGroup;
	}

	public void setAssignableGroup(String assignableGroup) {
		this.assignableGroup = assignableGroup;
	}

	public String getGroupAllowedToSwitch() {
		return groupAllowedToSwitch;
	}

	public void setGroupAllowedToSwitch(String groupAllowedToSwitch) {
		this.groupAllowedToSwitch = groupAllowedToSwitch;
	}

}
