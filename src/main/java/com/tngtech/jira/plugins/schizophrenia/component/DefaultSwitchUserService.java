package com.tngtech.jira.plugins.schizophrenia.component;

import java.security.Principal;
import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.jira.auditing.*;
import com.atlassian.jira.web.ExecutingHttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.tngtech.jira.plugins.schizophrenia.ao.SwitchUserConfig;
import com.tngtech.jira.plugins.schizophrenia.exception.NoUsePermissionException;
import com.tngtech.jira.plugins.schizophrenia.exception.UserNotAllowedToSwitchException;
import com.tngtech.jira.plugins.schizophrenia.exception.UserNotInPossibleUsersException;

@Component
@SuppressWarnings("unused")
public class DefaultSwitchUserService implements SwitchUserService {
    Logger log = LoggerFactory.getLogger(DefaultSwitchUserService.class);

    private static final String SESSION_ORIGINAL_USER = "original_user";

    @Autowired
    private UserManager userManager;

    @Autowired
    private GroupManager groupManager;

    @Autowired
    private PermissionManager permissionManager;

    @Autowired
    private SwitchUserConfigService switchUserConfigService;

    @Autowired
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Autowired
    private AuditingManager auditingManager;

    private UserInSessionPutter userInSessionPutter = new UserInSessionPutter();

    private class UserInSessionPutter extends DefaultAuthenticator {

        @Override
        protected Principal getUser(String username) {
            return userManager.getUserByName(username);
        }

        @Override
        protected boolean authenticate(Principal user, String password) throws AuthenticatorException {
            return false;
        }

        public void putUserIntoSession(ApplicationUser user, HttpServletRequest httpServletRequest) {
            putPrincipalInSessionContext(httpServletRequest, user);
        }
    }

    @Override
    public boolean mayUserSwitch(HttpServletRequest httpServletRequest) {
        try {
            checkUserIsAllowedToSwitch(getRealUser(httpServletRequest));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    @Override
    public void switchToUsername(ApplicationUser userToSwitchTo, HttpServletRequest httpServletRequest)
            throws NoUsePermissionException, UserNotAllowedToSwitchException, UserNotInPossibleUsersException {
        checkUserIsJiraUser(userToSwitchTo);
        checkUserIsInPossibleUsers(userToSwitchTo, httpServletRequest);

        ApplicationUser realUser = getRealUser(httpServletRequest);
        checkUserIsAllowedToSwitch(realUser);

        httpServletRequest.getSession().setAttribute(SESSION_ORIGINAL_USER, realUser);
        userInSessionPutter.putUserIntoSession(userToSwitchTo, httpServletRequest);

        logUserSwitch(userToSwitchTo, realUser);
    }

    private void logUserSwitch(final ApplicationUser userToSwitchTo, final ApplicationUser realUser) {
        String message;
        final ApplicationUser currentUser = jiraAuthenticationContext.getUser();

        if (realUser.equals(userToSwitchTo)) {
            message = String.format(
                    "user '%s' switched back to themselves from user '%s' (via User Switcher for JIRA)",
                    realUser.getUsername(),
                    currentUser.getUsername()
            );
        } else {
            message = String.format(
                    "user '%s' switched to user '%s' (via User Switcher for JIRA)",
                    realUser.getUsername(),
                    userToSwitchTo.getUsername()
            );
        }
        log.warn(message);

        String remoteAddress = ExecutingHttpRequest.get().getRemoteAddr();
        RecordRequest recordRequest = new RecordRequest(AuditingCategory.PERMISSIONS, message, realUser, remoteAddress)
                .thatAffects(new AffectedUser(userToSwitchTo), new AffectedUser(realUser))
                .withChangedValues(new ChangedValue() {
                    @Nonnull @Override
                    public String getName() {
                        return "Authenticated user";
                    }

                    @Nullable @Override
                    public String getFrom() {
                        return currentUser.getUsername();
                    }

                    @Nullable @Override
                    public String getTo() {
                        return userToSwitchTo.getUsername();
                    }
                });

        auditingManager.store(recordRequest);
    }

    private void checkUserIsJiraUser(ApplicationUser user) throws NoUsePermissionException {
        boolean hasUsePermission = permissionManager.hasPermission(Permissions.USE, user);
        if (!hasUsePermission) {
            throw new NoUsePermissionException(user);
        }
    }

    private void checkUserIsAllowedToSwitch(ApplicationUser user) throws UserNotAllowedToSwitchException {
        SwitchUserConfig switchUserConfig = switchUserConfigService.getValidConfiguration();
        Group group = groupManager.getGroup(switchUserConfig.getGroupAllowedToSwitch());

        if (group != null) {
            if (!groupManager.isUserInGroup(user.getUsername(), group.getName())) {
                throw new UserNotAllowedToSwitchException(user);
            }
        } else {
            if (!permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, user)) {
                throw new UserNotAllowedToSwitchException(user);
            }
        }
    }

    private void checkUserIsInPossibleUsers(ApplicationUser user, HttpServletRequest httpServletRequest)
            throws UserNotInPossibleUsersException {
        if (!getPossibleUsers(httpServletRequest).contains(user)) {
            throw new UserNotInPossibleUsersException(user);
        }
    }

    @Override
    public ApplicationUser getRealUser(HttpServletRequest httpServletRequest) {
        ApplicationUser user = (ApplicationUser) httpServletRequest.getSession().getAttribute(SESSION_ORIGINAL_USER);
        if (user == null) {
            return jiraAuthenticationContext.getUser();
        }
        return user;
    }

    @Override
    public Collection<ApplicationUser> getPossibleUsers(HttpServletRequest httpServletRequest) {
        Collection<ApplicationUser> possibleUsers = Lists.newArrayList();
        Collection<ApplicationUser> users = userManager.getAllApplicationUsers();

        SwitchUserConfig switchUserConfig = switchUserConfigService.getValidConfiguration();
        String assignableGroup = switchUserConfig.getAssignableGroup();
        if (!Strings.isNullOrEmpty(assignableGroup) && groupManager.groupExists(assignableGroup)) {
            possibleUsers.addAll(ApplicationUsers.from(groupManager.getUsersInGroup(assignableGroup)));
        } else {
            possibleUsers.addAll(users);
        }

        if(!possibleUsers.contains(getRealUser(httpServletRequest))) {
            possibleUsers.add(getRealUser(httpServletRequest));
        }
        return possibleUsers;
    }

}
