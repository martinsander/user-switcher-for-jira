package com.tngtech.jira.plugins.schizophrenia.rest;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.Lists;
import com.tngtech.jira.plugins.schizophrenia.ao.SwitchUserConfig;
import com.tngtech.jira.plugins.schizophrenia.component.SwitchUserConfigService;
import com.tngtech.jira.plugins.schizophrenia.component.SwitchUserService;
import com.tngtech.jira.plugins.schizophrenia.exception.NoUsePermissionException;
import com.tngtech.jira.plugins.schizophrenia.exception.NonExistingUserException;
import com.tngtech.jira.plugins.schizophrenia.exception.UserNotAllowedToSwitchException;
import com.tngtech.jira.plugins.schizophrenia.exception.UserNotInPossibleUsersException;
import com.tngtech.jira.plugins.schizophrenia.rest.model.MayUserSwitchModel;
import com.tngtech.jira.plugins.schizophrenia.rest.model.User;
import com.tngtech.jira.plugins.schizophrenia.rest.model.UserSwitchConfiguration;

@Path("/switchuser")
public class UserSwitch {

    @Autowired
    private SwitchUserService switchUserService;

    @Autowired
    private SwitchUserConfigService switchUserConfigService;

    @Autowired
    private ActiveObjects entityManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private GroupManager groupManager;

    @Autowired
    private AvatarService avatarService;

    @POST
    @Path("/switch/{username}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response switchUser(@PathParam("username") String username, @Context HttpServletRequest httpServletRequest) {
        try {
            ApplicationUser user = getCheckedApplicationUser(username);
            switchUserService.switchToUsername(user, httpServletRequest);
        } catch (NonExistingUserException exception) {
            return errorResponse(Status.NOT_FOUND, exception);
        } catch (NoUsePermissionException exception) {
            return errorResponse(Status.FORBIDDEN, exception);
        } catch (UserNotAllowedToSwitchException exception) {
            return errorResponse(Status.FORBIDDEN, exception);
        } catch (UserNotInPossibleUsersException exception) {
            return errorResponse(Status.FORBIDDEN, exception);
        } catch (Exception exception) {
            return errorResponse(Status.INTERNAL_SERVER_ERROR, exception);
        }

        return Response.noContent().build();
    }

    @PUT
    @Path("/configuration")
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response setUserSwitchConfiguration(UserSwitchConfiguration configuration) {
        try {
            switchUserConfigService.storeConfiguration(configuration.getGroupAllowedToSwitch(),
                    configuration.getAssignableGroup());
            return Response.ok(toUserSwitchConfiguration(switchUserConfigService.getValidConfiguration())).build();
        } catch (Exception exception) {
            return errorResponse(Status.INTERNAL_SERVER_ERROR, exception);
        }
    }

    @GET
    @Path("/configuration")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getUserSwitchConfiguration() {
        try {
            return Response.ok(toUserSwitchConfiguration(switchUserConfigService.getValidConfiguration())).build();
        } catch (Exception exception) {
            return errorResponse(Status.INTERNAL_SERVER_ERROR, exception);
        }
    }

    @GET
    @Path("possibleusers")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getPossibleUsers(@Context HttpServletRequest httpServletRequest) {
        Collection<User> usersModel = Lists.newArrayList();

        Collection<ApplicationUser> users = switchUserService.getPossibleUsers(httpServletRequest);
        for (ApplicationUser user : users) {
            usersModel.add(toUserModel(user));
        }
        
        return Response.ok(new UsernameListResponse(usersModel)).build();
    }

    @GET
    @Path("realuser")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getRealUser(@Context HttpServletRequest httpServletRequest) {
        ApplicationUser originalUser = switchUserService.getRealUser(httpServletRequest);

        if (originalUser != null) {
            return Response.ok(toUserModel(originalUser)).build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("userinformation/{username}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getUserInformation(@PathParam("username") String username) {
        try {
            return Response.ok(toUserModel(getCheckedApplicationUser(username))).build();
        } catch (NonExistingUserException exception) {
            return Response.status(Status.NOT_FOUND).build();
        } catch (Exception exception) {
            return errorResponse(Status.INTERNAL_SERVER_ERROR, exception);
        }
    }
    
    @GET
    @Path("/mayuserswitch")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response mayUserSwitch(@Context HttpServletRequest httpServletRequest) {
        try {
            return Response.ok(new MayUserSwitchModel(switchUserService.mayUserSwitch(httpServletRequest))).build();
        } catch (Exception exception) {
            return errorResponse(Status.INTERNAL_SERVER_ERROR, exception);
        }
    }

    private UserSwitchConfiguration toUserSwitchConfiguration(SwitchUserConfig switchUserConfig) {
        UserSwitchConfiguration userSwitchConfiguration = new UserSwitchConfiguration();
        userSwitchConfiguration.setAssignableGroup(switchUserConfig.getAssignableGroup());
        userSwitchConfiguration.setGroupAllowedToSwitch(switchUserConfig.getGroupAllowedToSwitch());
        return userSwitchConfiguration;
    }

    private User toUserModel(ApplicationUser user) {
        return new User(user.getUsername(), user.getDisplayName(), getAvatarUrl(user));
    }

    private String getAvatarUrl(ApplicationUser user) {
        return avatarService.getAvatarUrlNoPermCheck(user, Avatar.Size.defaultSize()).toString();
    }

    private ApplicationUser getCheckedApplicationUser(String username) throws NonExistingUserException {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null) {
            throw new NonExistingUserException(username);
        }
        return user;
    }

    private Response errorResponse(Status status, Exception exception) {
        return Response.status(status).entity(exception.getMessage()).type(MediaType.TEXT_PLAIN).build();
    }
}
