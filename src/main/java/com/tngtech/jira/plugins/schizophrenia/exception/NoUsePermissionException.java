package com.tngtech.jira.plugins.schizophrenia.exception;

import com.atlassian.jira.user.ApplicationUser;

public class NoUsePermissionException extends Exception {

	public NoUsePermissionException(ApplicationUser user) {
		super(String.format("User '%s' has not the use permission", user));
	}

}
