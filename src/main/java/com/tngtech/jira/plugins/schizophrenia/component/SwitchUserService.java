package com.tngtech.jira.plugins.schizophrenia.component;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.atlassian.jira.user.ApplicationUser;
import com.tngtech.jira.plugins.schizophrenia.exception.NoUsePermissionException;
import com.tngtech.jira.plugins.schizophrenia.exception.UserNotAllowedToSwitchException;
import com.tngtech.jira.plugins.schizophrenia.exception.UserNotInPossibleUsersException;

@Component
public interface SwitchUserService {
	void switchToUsername(ApplicationUser user, HttpServletRequest httpServletRequest) throws NoUsePermissionException, UserNotAllowedToSwitchException, UserNotInPossibleUsersException;
	
	ApplicationUser getRealUser(HttpServletRequest httpServletRequest);

    Collection<ApplicationUser> getPossibleUsers(HttpServletRequest httpServletRequest);

    boolean mayUserSwitch(HttpServletRequest httpServletRequest);
}
