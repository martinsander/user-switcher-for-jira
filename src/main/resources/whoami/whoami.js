
AJS.namespace('Schizophrenia.Whoami', window, Class.extend({
	
	showDialog: function() {
		var dialog = new AJS.Dialog({
				width: 600,
				height: 400,
				closeOnOutsideClick: true	
			}),
			schizophrenia = new Schizophrenia();
		
		schizophrenia.init(AJS.params.baseURL);
		
		dialog.addHeader(AJS.I18n.getText("schizophrenia.keyboard.shortcut.whoami.dialog.header"));
		dialog.addButton(AJS.I18n.getText("schizophrenia.keyboard.shortcut.whoami.dialog.button"), function() {
			dialog.hide();
		});
		
		schizophrenia.getRealUser(function (realuser) {
			schizophrenia.getUserInfo(AJS.params.loggedInUser, function (user) {		
				dialog.addPanel('SinglePanel', Schizophrenia.Templates.Whoami.dialog({
					user: {
						username: user.username,
						displayName: user.displayName,
						image: user.avatarUrl 
					},
					realuser: {
						username: realuser.username,
						displayName: realuser.displayName,
						image: realuser.avatarUrl 
					}
				}), 'singlePanel');
				dialog.show();
			}, function() {
				dialog.addPanel('SinglePanle', "I don't know either!", 'singlePanle');
				dialog.show();
			});
		});
	}

}));