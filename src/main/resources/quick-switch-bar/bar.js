
AJS.namespace('Schizophrenia.QuickSwitchBar', window, Class.extend({
	toggle: function() {
		AJS.$('#quick-switch-bar').toggle();
		AJS.Cookie.save('schizophrenia.quickswitchbar.visible', AJS.$('#quick-switch-bar').is(':visible'));
	}
}));

AJS.toInit(function() {
	var schizophrenia = new Schizophrenia();
	
	schizophrenia.init(AJS.params.baseURL);
	
	schizophrenia.getRealUser(createBar, function(user) {
		createBar(user);
	});
	
	function createBar(originalUser) {
		AJS.$.getJSON(schizophrenia.restApiUrl + '/switchuser/possibleusers', function(response) {
			var users = response.users;
			
			schizophrenia.sortUsers(users, originalUser || AJS.params.loggedInUser);
			AJS.$('#header').append(Schizophrenia.Templates.QuickSwitchBar.toolbar({
				currentUser: AJS.params.loggedInUser,
				originalUser: originalUser,
				users: users
			}));
			
			AJS.$('button.switch-user').click(function() {
				var username = AJS.$(this).attr('username');
				schizophrenia.switchToUser(username);
			});
			
			AJS.$('button.switch-to-origin').click(function() {
				if(originalUser) {
					schizophrenia.switchToUser(originalUser.username);
				}
			});
			
			if(AJS.Cookie.read('schizophrenia.quickswitchbar.visible') === 'true') {
				AJS.$('#quick-switch-bar').show();
			}
		});
	}
});


